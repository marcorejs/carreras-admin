
var config = {
    apiKey: "AIzaSyCQk8dT3Px1moCZ6tDfGDC1zsNTO1q3dVk",
    authDomain: "administrador-espacios.firebaseapp.com",
    databaseURL: "https://administrador-espacios.firebaseio.com",
    projectId: "administrador-espacios",
    storageBucket: "administrador-espacios.appspot.com",
    messagingSenderId: "250008479103"
};
firebase.initializeApp(config);

var elem = document.getElementById('draw-shapes');
var params = { width: 1200, height: 2000 };
var two = new Two(params).appendTo(elem);
var rectangulos = [];
var tarifas = [
    {
        nombre: "normal",
        hora: 18,
        fraccion: 8,
        t_fraccion: 15,
        hora_gratis: false
    },
    {
        nombre: "negocio",
        hora: 15,
        fraccion: 7,
        t_fraccion: 15,
        hora_gratis: true
    }
];

var margin = { right: 0, top: 0 };
var db = firebase.firestore();
var doc_ref = db.doc("estacionamientos/yLcxIVpBvnolliDh0XXz");
cajones = [];
var bandera = false;
var columnas = 6;

function generarCajones() {
    var num_espacios = parseInt(document.getElementById("tb_espacios").value);
    for (var i = 0; i < num_espacios; i++) {
        var text = two.makeText("hola", 100 + margin.right, 100 + margin.top, "normal");
        var cobro = two.makeText("0", 100 + margin.right, 120 + margin.top, "normal");
        var hora = two.makeText("0", 100 + margin.right, 80 + margin.top, "normal");
        var rect = two.makeRectangle(100 + margin.right, 100 + margin.top, 200, 150);
        rect.fill = "rgb(70,200,70)";
        rectangulos.push(rect);
        margin.right = margin.right + 200;
        var cajon = {
            rectangulo: rect,
            tiempo: text,
            horas: 0,
            minutos: 0,
            precio_vista: cobro,
            precio: 0,
            hora_ingreso: hora,
            contador_h: 0,
            contador_s: 0,
            contador_m: 0,
            fracciones: 0,
            tarifa_seleccionada: "normal"
        }
        cajones.push(cajon);
        if (cajones.length % columnas == 0) {
            margin.top = margin.top + 150;
            margin.right = 0
        }
    }

    two.play();
    two.update();

    cajones.forEach(cajon => {
        $(cajon.rectangulo._renderer.elem)
            .css('cursor', 'pointer')
            .click(function (e) {
                $('#modal1').modal('open');
                if (cajon.rectangulo.fill == "rgb(70,200,70)") {
                    var aux;
                    cajon.rectangulo.noFill();
                    doc_ref.get().then(doc => {
                        aux = parseInt(doc.data().espacios) - 1;
                        doc_ref.update({
                            espacios: aux
                        })
                    })
                    
                    
                    cajon.tiempo.value = cajon.contador_h + ":" + cajon.contador_m + ":" + cajon.contador_s;
                    fecha = new Date();
                    cajon.hora_ingreso.value = "Ingreso: " + fecha.getHours() + ":" + fecha.getMinutes() + ":" + fecha.getSeconds();

                    cronometro = setInterval(
                        function () {
                            if (bandera) {
                                //Buscar tarifa en el array de tarifas
                                cajon.tarifa_seleccionada = document.getElementById("sel_tarifa").value;
                                tarifas.forEach(tarifa => {
                                    if (tarifa.nombre == cajon.tarifa_seleccionada) {
                                        cajon.tarifa_seleccionada = tarifa;
                                    }
                                });
                                //Inicia el cobro de tiempo
                                if (cajon.contador_s >= 59) {
                                    cajon.contador_s = 0;
                                    cajon.contador_m++;
                                    cajon.minutos = cajon.contador_m;
                                    if (cajon.contador_m >= 60) {
                                        cajon.contador_m = 0;
                                        cajon.contador_h++;
                                        cajon.horas = cajon.contador_h;
                                        cajon.fracciones = 0;
                                    } else if ((cajon.contador_m % cajon.tarifa_seleccionada.t_fraccion) == 0 && cajon.fracciones < 2 && cajon.fracciones != 0) {
                                        cajon.fracciones++;
                                    } else if ((cajon.contador_m % (cajon.tarifa_seleccionada.t_fraccion * 2)) == 0){
                                        cajon.fracciones++;
                                    }
                                    cajon.tiempo.value = cajon.contador_h + ":" + cajon.contador_m + ":" + cajon.contador_s;
                                } else {
                                    cajon.contador_s+= 60;
                                    cajon.tiempo.value = cajon.contador_h + ":" + cajon.contador_m + ":" + cajon.contador_s;
                                }
                                cajon.precio = (cajon.tarifa_seleccionada.hora * cajon.contador_h) + (cajon.tarifa_seleccionada.fraccion * cajon.fracciones);
                                cajon.precio_vista.value = "Monto total: " + cajon.precio + " MXN";
                            }
                        }, 1000);

                } else {
                    cajon.rectangulo.fill = "rgb(70,200,70)";
                    var aux;
                    doc_ref.get().then(doc => {
                        aux = parseInt(doc.data().espacios) + 1;
                        doc_ref.update({
                            espacios: aux
                        })
                    })
                    alert("Cobrar: " + cajon.precio + " MXN");
                }
            });
    })
}

$(document).ready(function () {
    $('.modal').modal();
});

function comenzarCobro() {
    bandera = true;
}
