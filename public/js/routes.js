var accion = "ruta";
var icono;
var icono_app;
var titulo_servicio;
var contenido_servicio;
var titulo_anunciante;
var contenido_anunciante;
var distancia_ruta = 0;
var servicios_hidratacion = [ 
    { nombre: "Aguarama", icono: "https://firebasestorage.googleapis.com/v0/b/administrador-espacios.appspot.com/o/aguarama.png?alt=media&token=a192d013-4b62-456c-b0db-3ac2d303cf7d" },
    { nombre: "Gatorade", icono: "https://firebasestorage.googleapis.com/v0/b/administrador-espacios.appspot.com/o/gatorade.png?alt=media&token=79ab7bc6-57ab-4976-ab7f-cb91c1e5633c" },
    { nombre: "Electrolit", icono: "" },
    { nombre: "Cruz roja", icono: "https://firebasestorage.googleapis.com/v0/b/administrador-espacios.appspot.com/o/cruz_roja.png?alt=media&token=0b1b0881-6b26-4c94-9711-c84752192680" }
];
var servicios_medicos = [{ nombre: "Cruz roja", icono: "https://firebasestorage.googleapis.com/v0/b/administrador-espacios.appspot.com/o/cruz_roja.png?alt=media&token=0b1b0881-6b26-4c94-9711-c84752192680"}];
var servicio_requerido = {};
var servicios_globales = servicios_medicos + servicios_hidratacion;
var estacionamientos = [];
var estacionamiento;
var contador_trazo = 0;
var nueva_linea;
var trazo;


var config = {
    apiKey: "AIzaSyCQk8dT3Px1moCZ6tDfGDC1zsNTO1q3dVk",
    authDomain: "administrador-espacios.firebaseapp.com",
    databaseURL: "https://administrador-espacios.firebaseio.com",
    projectId: "administrador-espacios",
    storageBucket: "administrador-espacios.appspot.com",
    messagingSenderId: "250008479103"
};
firebase.initializeApp(config);

var db = firebase.firestore();
var puntos_ruta = [];
var servicios = [];
var anunciantes = [];
var auxiliar_info = {};
var info_servicio = "";
var map;


function initMap() {
    var durango = { lat: 24.028596212016996, lng: -104.66319203292852 };
    map = new google.maps.Map(document.getElementById('map'), {
        zoom: 15,
        center: durango,
        styles: [{
            featureType: 'poi',
            stylers: [{ visibility: 'off' }]
        }, {
            featureType: 'transit.station',
            stylers: [{ visibility: 'off' }]
        }]
    });

    var marcadores = [];
    
    map.addListener("click", addPoint)
    

    poly = new google.maps.Polyline({
        strokeColor: '#000000',
        strokeOpacity: 1.0,
        strokeWeight: 3
    });

    poly.setMap(map);


    function addPoint(event) {
        if(accion == "ruta"){
            var path = poly.getPath();

            path.push(event.latLng);

            if (marcadores.length == 0) {
                var marker = new google.maps.Marker({
                    position: event.latLng,
                    title: '#' + path.getLength(),
                    map: map,
                    label: "Meta"
                });

                marcadores.push(marker);
            } else {
                var start = {
                    lat: marcadores[0].getPosition().lat(),
                    lng: marcadores[0].getPosition().lng()
                }
                var end = {
                    lat: event.latLng.lat(),
                    lng: event.latLng.lng()
                }
                
                distancia_ruta = distancia_ruta + getDistanceBetweenPoints(start, end, 'km');
                console.log(distancia_ruta);
                document.getElementById("distanciaCarrera").innerHTML = "Aproximadamente " + distancia_ruta.toFixed(2) + " km";
                marcadores[0].setMap(null);
                marcadores.pop();
                var marker = new google.maps.Marker({
                    position: event.latLng,
                    title: '#' + path.getLength(),
                    map: map,
                    label: "Meta"
                });

                marcadores.push(marker);
            }
            var marker_latlng = {
                lat: marker.getPosition().lat(),
                lng: marker.getPosition().lng()
            }
            puntos_ruta.push(marker_latlng);
            console.log(marker_latlng);
        } else if(accion == "servicio"){
            console.log(servicio_requerido);
            var image = {
                url: servicio_requerido.icono,
                size: new google.maps.Size(120, 120),
                origin: new google.maps.Point(0, 0),
                anchor: new google.maps.Point(0, 32),
                scaledSize: new google.maps.Size(40, 40),
            }

            var marker = new google.maps.Marker({
                position: event.latLng,
                map: map,
                icon: image,
                draggable: true
            })

            var servicio = {
                lat: marker.getPosition().lat(),
                lng: marker.getPosition().lng(),
                info: contenido_servicio,
                titulo: titulo_servicio,
                icon: servicio_requerido.icono
            }

            var infowindow = new google.maps.InfoWindow({
                content: "<h6>" + titulo_servicio + "</h6>"
            });

            marker.addListener('click', function () {
                infowindow.open(map, marker);
            });
            marker.addListener("dragstart", function (event) {
                servicios.forEach(servicio => {
                    console.log(servicio.lat);
                    console.log(marker.getPosition().lat())
                    if (servicio.lat == marker.getPosition().lat()) {
                        servicios.splice(servicios.indexOf(servicio), 1);
                        auxiliar_info = {
                            info: servicio.info,
                            titulo: servicio.titulo,
                            icon: servicio.icon,
                        }
                    }
                });
                console.log(servicios);
            });

            marker.addListener("dragend", function (event) {
                var servicio = {
                    lat: marker.getPosition().lat(),
                    lng: marker.getPosition().lng(),
                    info: auxiliar_info.info,
                    titulo: auxiliar_info.titulo,
                    icon: auxiliar_info.icon
                }
                servicios.push(servicio);
                console.log(servicios);
            });

            servicios.push(servicio);
        } else if(accion == "anunciante"){
            var image = {
                url: "https://firebasestorage.googleapis.com/v0/b/administrador-espacios.appspot.com/o/monchys.png?alt=media&token=280aee16-c6ce-447b-8372-5d4b43f63fce",
                size: new google.maps.Size(120, 120),
                origin: new google.maps.Point(0, 0),
                anchor: new google.maps.Point(0, 32),
                scaledSize: new google.maps.Size(80, 60),
            }

            var marker = new google.maps.Marker({
                position: event.latLng,
                map: map,
                icon: image,
                draggable: true
            })

            var anunciante = {
                lat: marker.getPosition().lat(),
                lng: marker.getPosition().lng(),
                info: "monchys",
                titulo: "monchys",
                icon: "https://firebasestorage.googleapis.com/v0/b/administrador-espacios.appspot.com/o/monchys.png?alt=media&token=280aee16-c6ce-447b-8372-5d4b43f63fce"
            }

            marker.addListener("dragstart", function (event) {
                anunciantes.forEach(anunciante => {
                    console.log(anunciante.lat);
                    console.log(marker.getPosition().lat())
                    if (anunciante.lat == marker.getPosition().lat()) {
                        anunciantes.splice(anunciantes.indexOf(anunciante), 1);
                        auxiliar_info = {
                            info: anunciante.info,
                            titulo: anunciante.titulo,
                            icon: anunciante.icon,
                        }
                    }
                });
                console.log(anunciantes);
            });

            marker.addListener("dragend", function (event) {
                var anunciante = {
                    lat: marker.getPosition().lat(),
                    lng: marker.getPosition().lng(),
                    info: auxiliar_info.info,
                    titulo: auxiliar_info.titulo,
                    icon: auxiliar_info.icon
                }
                anunciantes.push(anunciante);
                console.log(anunciantes);
            });

            anunciantes.push(anunciante);
        } else if(accion == "estacionamiento"){
            if(contador_trazo == 0){
                estacionamientos.push(estacionamiento);
                console.log(estacionamiento);
                estacionamiento = {
                    puntos: []
                }
                nueva_linea = aestacionamiento();
                trazo = nueva_linea.getPath();
                trazo.push(event.latLng);
                console.log(trazo);
                contador_trazo++;
            } else {
                var trazo = nueva_linea.getPath();
                trazo.push(event.latLng);
                console.log(trazo);
            }
            estacionamiento.puntos.push({ lat: event.latLng.lat(), lng: event.latLng.lng() });
        }
        
        
    }

}

function servicio(){
    var selector_servicio = document.getElementById("select-servicio").value;
    titulo_servicio = document.getElementById("titulo-servicio").value;
    contenido_servicio = document.getElementById("contenido-servicio").value;
    accion = "servicio";
    servicios_hidratacion.forEach(servicio_h => {
        if(servicio_h.nombre == titulo_servicio){
            servicio_requerido = servicio_h;
        }
    })
}

function anunciante(){
    accion = "anunciante";
}

function selectorIconoServicio(){
    limpiarSelect();
    var selector_servicio = document.getElementById("select-servicio").value;
    if(selector_servicio == "hidratacion"){
        var i = 0;
        servicios_hidratacion.forEach(s_hidratacion => {
            if(i == 0){
                var hidratacion_template = '<option value="' + s_hidratacion.nombre + '" selected>' + s_hidratacion.nombre + '</option>';
            } else {
                var hidratacion_template = '<option value="' + s_hidratacion.nombre + '">' + s_hidratacion.nombre + '</option>'
            }
            $(hidratacion_template).appendTo("#titulo-servicio");
            i++;
        });
    } else if(selector_servicio == "medico"){
        var i = 0;
        servicios_medicos.forEach(s_medico => {
            if (i == 0) {
                var medicos_template = '<option value="' + s_medico.nombre + '" selected>' + s_medico.nombre + '</option>';
            } else {
                var medicos_template = '<option value="' + s_medico.nombre + '">' + s_medico.nombre + '</option>'
            }
            $(medicos_template).appendTo("#titulo-servicio");
            i++;
        });
    }
}

function limpiarSelect(){

}

function ruta(){
    accion = "ruta";
}
function aestacionamiento(){
    contador_trazo = 0;
    accion = "estacionamiento";
    var poli = new google.maps.Polyline({
        strokeColor: "#FA2222",
        strokeOpacity: 1.0,
        strokeWeight: 3
    })
    poli.setMap(map);
    return poli
}

function guardarRuta(){
    estacionamientos.shift();
    var titulo = document.getElementById('titulo-carrera').value;
    var carrera = {
        ruta: puntos_ruta,
        nombre: titulo,
        servicios: servicios,
        costo: "150 MXN",
        anunciantes: anunciantes,
        estacionamientos: estacionamientos
    }
    console.log(carrera)
    db.collection("carreras").add(carrera);
}

function getDistanceBetweenPoints(start, end, units) {

    let earthRadius = {
        miles: 3958.8,
        km: 6371
    };

    var R = earthRadius[units || 'km'];
    var lat1 = start.lat;
    var lon1 = start.lng;
    var lat2 = end.lat;
    var lon2 = end.lng;

    var dLat = this.toRad((lat2 - lat1));
    var dLon = this.toRad((lon2 - lon1));
    var a = Math.sin(dLat / 2) * Math.sin(dLat / 2) +
        Math.cos(this.toRad(lat1)) * Math.cos(this.toRad(lat2)) *
        Math.sin(dLon / 2) *
        Math.sin(dLon / 2);
    var c = 2 * Math.atan2(Math.sqrt(a), Math.sqrt(1 - a));
    var d = R * c;

    return d;

}

function toRad(x) {
    return x * Math.PI / 180;
}



