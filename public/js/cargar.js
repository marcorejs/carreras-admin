var config = {
    apiKey: "AIzaSyCQk8dT3Px1moCZ6tDfGDC1zsNTO1q3dVk",
    authDomain: "administrador-espacios.firebaseapp.com",
    databaseURL: "https://administrador-espacios.firebaseio.com",
    projectId: "administrador-espacios",
    storageBucket: "administrador-espacios.appspot.com",
    messagingSenderId: "250008479103"
};
firebase.initializeApp(config);

var db = firebase.firestore();
var ruta = [];
var servicios = [];

function initMap() {
    var durango = { lat: 24.028596212016996, lng: -104.66319203292852 };
    var map = new google.maps.Map(document.getElementById('map'), {
        zoom: 16,
        center: durango
    });

    db.collection("carreras").get().then((querySnapshot) => {
        querySnapshot.forEach((doc) => {
            ruta = doc.data().ruta;
            servicios = doc.data().servicios;
        });
        servicios.forEach(servicio => {
            if(servicio.info == "Hidratacion"){
                var image = {
                    url: "../public/img/w_drop.png",
                    size: new google.maps.Size(120, 120),
                    origin: new google.maps.Point(0, 0),
                    anchor: new google.maps.Point(0, 32),
                    scaledSize: new google.maps.Size(60, 60),
                }
                var marker = new google.maps.Marker({
                    position: { lat: servicio.lat, lng: servicio.lng },
                    map: map,
                    icon: image
                });
            }
        })
        var ruta_carrera = new google.maps.Polyline({
            path: ruta,
            geodesic: true,
            strokeColor: '#0000FF',
            strokeOpacity: 1.0,
            strokeWeight: 2
        });

        ruta_carrera.setMap(map);
    });

}