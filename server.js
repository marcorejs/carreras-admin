var http = require('http');
var fs = require('fs');

http.createServer(function (req, res) {
    if (req.url === '/index.html') {
        fs.createReadStream(__dirname + 'public/index.html').pipe(res)
    } else {
        fs.createReadStream(__dirname + 'public/espacios.html').pipe(res)
    }
}).listen(3000)